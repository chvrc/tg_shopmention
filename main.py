from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.utils import executor
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
import gspread


BOT_TOKEN = "1675870180:AAGlk22K8pTZuEdma1dy8ybp4ucatsh0d54"

API_TOKEN = BOT_TOKEN

bot = Bot(token=API_TOKEN)

storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


class Mention(StatesGroup):
    start = State()
    q1 = State()
    q2 = State()
    q3 = State()
    q4 = State()
    q5 = State()

ans = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
qs = ["1\. *Какова вероятность того, что Вы порекомендуете магазин своим знакомым?*\nпо 10\-бальной шкале\n_0 — «Ни в коем случае не буду рекомендовать»_\n_7 — «Скорее всего порекомендую»_\n_10 — «Обязательно порекомендую»_",
    "2\. *Как Вы оцените ассортимент магазина?*\nпо 10\-балльной шкале\n_0 — «Ужасно»_\n_7 — «Нормально»_\n_10 — «Очень хороший»_",
    "3\. *Как Вы оцените обслуживание в  магазине?*\nпо 10\-балльной шкале\n_0 — «Ужасно»_\n_7 — «Нормально»_\n_10 — «Очень хорошее»_",
    "4\. *Как Вам уровень цен в магазине?*\nпо 10\-балльной шкале\n_0 — «Очень дорого»_\n_7 — «Нормальные»_\n_10 — «Отличные»_",
    "5\. *Что нам улучшить, чтобы Вы рекомендовали нас Вашим знакомым?*\n_\(напишите ваш ответ ниже\)_"]


async def inline_kb():
    bt0 = InlineKeyboardButton('0🤮', callback_data='bt0')
    bt1 = InlineKeyboardButton('1', callback_data='bt1')
    bt2 = InlineKeyboardButton('2', callback_data='bt2')
    bt3 = InlineKeyboardButton('3', callback_data='bt3')
    bt4 = InlineKeyboardButton('4', callback_data='bt4')
    bt5 = InlineKeyboardButton('5', callback_data='bt5')
    bt6 = InlineKeyboardButton('6', callback_data='bt6')
    bt7 = InlineKeyboardButton('7😚', callback_data='bt7')
    bt8 = InlineKeyboardButton('8', callback_data='bt8')
    bt9 = InlineKeyboardButton('9', callback_data='bt9')
    bt10 = InlineKeyboardButton('10😍', callback_data='bt10')
    inline_kb = InlineKeyboardMarkup()
    inline_kb.row(bt0, bt1, bt2, bt3, bt4, bt5)
    inline_kb.row(bt6, bt7, bt8, bt9, bt10)
    return inline_kb


async def append_google_sheet(arr):
    gc = gspread.service_account(filename='credentials.json')
    sh = gc.open_by_key('1IqNKiCJVGe__PHDbnU5CUvLL7LWtjR8mRYnnUiffJm4')
    ws = sh.sheet1
    ws.append_row(arr, value_input_option='USER_ENTERED')


@dp.message_handler(commands=['start'])
async def on_start(message: types.Message, state: FSMContext):
    button_start = InlineKeyboardButton('Начать!', callback_data='start')
    kb = InlineKeyboardMarkup()
    kb.add(button_start)

    await Mention.start.set()
    await message.reply("Привет\!\n*Предлагаю пройти небольшой опрос \(15 сек\)\!*", reply=False, reply_markup=kb, parse_mode='MarkdownV2')

@dp.callback_query_handler(text='start', state=Mention.start)
async def question1(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()

    kb = await inline_kb()
    await Mention.q1.set()
    await call.message.edit_text(qs[0], reply_markup=kb, parse_mode='MarkdownV2')


@dp.callback_query_handler(state=Mention.q1)
async def question2(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()
    async with state.proxy() as data:
        data['q1'] = call.data[2:]
    kb = await inline_kb()
    await Mention.q2.set()
    await call.message.edit_text(qs[1], reply_markup=kb, parse_mode='MarkdownV2')

@dp.callback_query_handler(state=Mention.q2)
async def question3(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()
    async with state.proxy() as data:
        data['q2'] = call.data[2:]
    kb = await inline_kb()
    await Mention.q3.set()
    await call.message.edit_text(qs[2], reply_markup=kb, parse_mode='MarkdownV2')

@dp.callback_query_handler(state=Mention.q3)
async def question4(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()
    async with state.proxy() as data:
        data['q3'] = call.data[2:]
    kb = await inline_kb()
    await Mention.q4.set()
    await call.message.edit_text(qs[3], reply_markup=kb, parse_mode='MarkdownV2')

@dp.callback_query_handler(state=Mention.q4)
async def question5(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()
    async with state.proxy() as data:
        data['q4'] = call.data[2:]
    await Mention.q5.set()
    await call.message.edit_text(qs[4], parse_mode='MarkdownV2')


@dp.message_handler(content_types=['text'], state=Mention.q5)
async def add_photo(message: types.Message, state: FSMContext):
    await message.reply("*Спасибо за отзыв!*\nПокажите экран смартфона кассиру и заберите подарок 🎁\n_(билет на «миллион» лотереи «Сто лото» или чупа-чупс)_", reply=False, parse_mode='Markdown')
    async with state.proxy() as data:
        data['q5'] = message.text
    dt = str(message.date).split(' ')
    await append_google_sheet((dt[0], dt[1], int(data['q1']), 
                                int(data['q2']), int(data['q3']), int(data['q4']), 
                                data['q5'], message.from_user.id))
    await state.finish()


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)

